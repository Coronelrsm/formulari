package cat.itb.userform;

import android.content.Context;
import android.widget.DatePicker;

import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.contrib.PickerActions;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);
    @Test
    public void useAppContext() {
        // Context of the app under test.

        /*
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        assertEquals("cat.itb.userform", appContext.getPackageName());

        onView(withId(R.id.login))
                .perform(click())
                .check(matches(isDisplayed()));
        onView(allOf(withId(R.id.login), withText("Jelou!")));


        matches(withText(""));     // Té el text someText
        matches(withText(""));            // No té text
        matches(not(withText("")));        // Té algun text

         */

    }

    /*
        Test de login correcte.
        Test de login sense dades mostra errors
        Test de registre correcte.
        Test de registre incorrecte mostra errors
        Test de navegació login<->registre
     */

    @Test
    public void testLoginCorrecte() {

        //-----------Welcome Screen----------------//
        onView(withId(R.id.login))
                .perform(click())
                .check(matches(isDisplayed()));


        //-----------Login Screen------------------//
        onView(withId(R.id.usernameLogin)).perform(replaceText("Sisebuto666"));
        onView(withId(R.id.usernameLogin)).check(matches(withText("Sisebuto666")));

        onView(withId(R.id.passwordLogin)).perform(replaceText("11111"));
        onView(withId(R.id.passwordLogin)).check(matches(withText("11111")));

        onView(withId(R.id.login))
                .perform(click());

    }


    @Test
    public void testLoginError(){
        //-----------Welcome Screen----------------//
        onView(withId(R.id.login))
                .perform(click())
                .check(matches(isDisplayed()));


        //-----------Login Screen------------------//
        onView(withId(R.id.usernameLogin)).perform(replaceText(""));
        onView(withId(R.id.usernameLogin)).check(matches(withText("")));

        onView(withId(R.id.passwordLogin)).perform(replaceText(""));
        onView(withId(R.id.passwordLogin)).check(matches(withText("")));

        onView(withId(R.id.login))
                .perform(click())
                .check(matches(isDisplayed()));  //no peta perquè login està en el següent fragment també

    }

    @Test
    public void testRegistreCorrecte() {

        //-----------Welcome Screen----------------//
        onView(withId(R.id.register))
                .perform(click());
               // .check(matches(isDisplayed())); //peta perquè el botó registre no apaerix en el següent fragment

        //-----------Register Screen---------------//

        onView(withId(R.id.usernameRegister)).perform(replaceText("Sisebuto666"));
        onView(withId(R.id.passwordRegister)).perform(replaceText("11111"));
        onView(withId(R.id.repeatPasswordRegister)).perform(replaceText("11111"));
        onView(withId(R.id.emailRegister)).perform(replaceText("sisebuto@itb.cat"));
        onView(withId(R.id.nameRegister)).perform(replaceText("Sisebuto"));
        onView(withId(R.id.surnamesRegister)).perform(replaceText("Gumersindo de Asís"));

        //-----------Pickers que no funcionen--------------//
        //onView(withId(R.id.birthDate)).perform(PickerActions.setDate(2017, 6, 30));
        //onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(1970, 5, 20));
        //onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(1984, 10, 23));


        onView(withId(R.id.register))
                .perform(scrollTo(), click());  //si un element no és visible, cal fer scroll fins a arribar-hi

    }
    @Test
    public void testLoginIncorrecte(){
        //-----------Welcome Screen----------------//
        onView(withId(R.id.register))
                .perform(click());
        // .check(matches(isDisplayed())); //peta perquè el botó registre no apaerix en el següent fragment

        //-----------Register Screen---------------//

        onView(withId(R.id.usernameRegister)).perform(replaceText(""));
        onView(withId(R.id.passwordRegister)).perform(replaceText(""));
        onView(withId(R.id.repeatPasswordRegister)).perform(replaceText("22222"));
        onView(withId(R.id.emailRegister)).perform(replaceText("itb.cat"));
        onView(withId(R.id.nameRegister)).perform(replaceText("Sisebuto"));
        onView(withId(R.id.surnamesRegister)).perform(replaceText("Gumersindo de Asís"));

        onView(withId(R.id.register))
                .perform(scrollTo(), click());  //si un element no és visible, cal fer scroll fins a arribar-hi


    }

    @Test
    public void navegacioLoginRegistre(){
        //-----------Welcome Screen----------------//
        onView(withId(R.id.login))
                .perform(click());
        for (int i = 0; i< 5; i++) {
            recursivitat();
        }

    }

    private void recursivitat() {
        onView(withId(R.id.register))
                .perform(click());
        onView(withId(R.id.login))
                .perform(scrollTo(), click());  //si un element no és visible, cal fer scroll fins a arribar-hi
    }


}
