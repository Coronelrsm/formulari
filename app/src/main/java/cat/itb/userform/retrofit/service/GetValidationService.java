package cat.itb.userform.retrofit.service;

import cat.itb.userform.model.Validation;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface GetValidationService {




        @GET("/{nomusuari}/login.json")
        Call<Validation> login(@Path("nomusuari") String nomUsuari);

        @GET("/{nomusuari}/register.json")
        Call<Validation> register(@Path("nomusuari") String nomUsuari);

}
