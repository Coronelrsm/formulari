package cat.itb.userform.retrofit.instance;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ValidationInstance {

    private static Retrofit retrofit;
    private static final String BASE_URL = "http://itb.mateuyabar.com/DAM-M07/UF1/exercicis/userform/fakeapi/";

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}