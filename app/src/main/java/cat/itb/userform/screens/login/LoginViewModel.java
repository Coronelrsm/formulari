package cat.itb.userform.screens.login;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import cat.itb.userform.model.Validation;
import cat.itb.userform.retrofit.instance.ValidationInstance;
import cat.itb.userform.retrofit.service.GetValidationService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginViewModel extends ViewModel {



    private MutableLiveData<Boolean> logged = new MutableLiveData<>(false);
    private MutableLiveData<Boolean> loading = new MutableLiveData<>(false);
    private MutableLiveData<String> error = new MutableLiveData<>();


    private GetValidationService validationApi;
    private static final String BASE_URL = "http://itb.mateuyabar.com/DAM-M07/UF1/exercicis/userform/fakeapi/";

    public LoginViewModel(){
        Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        validationApi = retrofit.create(GetValidationService.class);

    }

    public MutableLiveData<Boolean> getLogged() {
        return logged;
    }

    public MutableLiveData<Boolean> getLoading() {
        return loading;
    }

    public MutableLiveData<String> getError() {
        return error;
    }

    public void getValidationLogin(String username, String password){
        logged.setValue(true);
        validationApi.login(username).enqueue(new Callback<Validation>() {

            @Override
            public void onResponse(Call<Validation> call, Response<Validation> response) {
                Validation validation = response.body();
                if(validation==null)
                    error.postValue("Error desconegut");
                else if(validation.isLogged()) {
                    logged.postValue(true);
                } else {
                    error.postValue(validation.getError());
                }
                loading.postValue(false);
            }

            @Override
            public void onFailure(Call<Validation> call, Throwable t) {
                error.postValue("Error desconegut");
                loading.setValue(false);
                t.printStackTrace();

            }
        });
    }

    public void getValidationRegister(String nomusuari){
        logged.setValue(true);
        validationApi.register(nomusuari).enqueue(new Callback<Validation>() {

            @Override
            public void onResponse(Call<Validation> call, Response<Validation> response) {
                if (response.isSuccessful()){
                    loading.setValue(false);
                }
            }

            @Override
            public void onFailure(Call<Validation> call, Throwable t) {
                loading.setValue(false);
                error.setValue("Registre incorrecte!");


            }
        });
    }


}

/*

View Model
MutableLiveData<boolean> loading = (...)

void login(){
loading.setValue(true);
userApi.call {
onResponse{
loading.setValue(false)
}
onFailure{
loading.setValue(false)
}
}
}


FRAGMENT

onActivityCreated
viewModel = -------------
viewModel.getLoading.observe(this, this:onLoadingChanged)

onLoadingChanged(){
if(loading){
//posar icona de carregant
}
}

 */