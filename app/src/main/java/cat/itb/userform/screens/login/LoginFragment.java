package cat.itb.userform.screens.login;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.userform.R;

public class LoginFragment extends Fragment {

    @BindView(R.id.backgroundImage)
    ImageView backgroundImage;
    @BindView(R.id.loginMessage)
    TextView loginMessage;
    @BindView(R.id.username)
    TextInputLayout username;
    @BindView(R.id.password)
    TextInputLayout password;
    @BindView(R.id.login)
    MaterialButton login;
    @BindView(R.id.register)
    MaterialButton register;
    @BindView(R.id.forgotPassword)
    MaterialButton forgotPassword;
    @BindView(R.id.usernameLogin)
    TextInputEditText usernameLogin;
    @BindView(R.id.passwordLogin)
    TextInputEditText passwordLogin;
    ProgressDialog dialog;
    private LoginViewModel mViewModel;
    private String errorSnack = "";

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        mViewModel.getLoading().observe(getViewLifecycleOwner(), this::onLoadingChanged);
        mViewModel.getError().observe(getViewLifecycleOwner(), this::onErrorChanged);
        mViewModel.getLogged().observe(getViewLifecycleOwner(), this::onLoggedChanged);
    }

    private void onLoggedChanged(Boolean logged) {
        if(logged){
            onViewClicked(this.getView());
        }
        else{
            displaySnackBarError();
        }

    }

    private void onErrorChanged(String error) {
        errorSnack = error;

    }



    private void onLoadingChanged(Boolean aBoolean) {
        if(aBoolean) {
            dialog = ProgressDialog.show(getContext(), "",
                    "Loading...", true);
            dialog.show();
        } else {
            if(dialog != null)
                dialog.dismiss();
        }

        }



    @OnClick({R.id.login, R.id.register, R.id.forgotPassword})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login:
                mViewModel.getValidationLogin(username.toString(), password.toString());
                navigateToLogged();
                break;
            case R.id.register:
                navigateToRegister();
                break;
            case R.id.forgotPassword:
                break;
        }
    }

    private void navigateToLogged() {

        //mViewModel
        validation();

    }

    private void displaySnackBarError() {
        Snackbar snackbar = Snackbar
                .make(this.getView(), errorSnack, Snackbar.LENGTH_LONG);
        snackbar.show();
    }

    private void validation() {
        boolean validate = true;

        refreshErrors();
        validate = validateIfEmpty(username, "El camp username està buit", validate);
        validate = validateIfEmpty(password, "El camp contrasenya està buit", validate);
        if (validate) {
            mViewModel.getValidationLogin(username.toString(), password.toString());
            proceedToNavigate();

        }
    }

    private boolean validateIfEmpty(TextInputLayout input, String error, boolean validate) {
        if (input.getEditText().getText().toString().isEmpty()) {
            validate = false;
            input.setError(error);
            scrollTo(input);

        }
        return validate;
    }

    private void scrollTo(TextInputLayout targetView) {
        targetView.getParent().requestChildFocus(targetView, targetView);

    }

    private void proceedToNavigate() {
        NavDirections action = LoginFragmentDirections.toLoggedFromLogin();
        Navigation.findNavController(getView()).navigate(action);
    }


    private void refreshErrors() {
        username.setError("");
        password.setError("");
    }

    private void navigateToRegister() {
        NavDirections action = LoginFragmentDirections.goToRegister();
        Navigation.findNavController(getView()).navigate(action);
    }
    /*

FRAGMENT

onActivityCreated
viewModel = -------------
viewModel.getLoading.observe(this, this:onLoadingChanged)

onLoadingChanged(){
if(loading){
//posar icona de carregant
}
}
     */
}
