package cat.itb.userform.screens.register;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.google.android.material.button.MaterialButton;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.userform.R;
import cat.itb.userform.screens.login.LoginViewModel;

public class RegisterFragment extends Fragment {

    @BindView(R.id.backgroundImage)
    ImageView backgroundImage;
    @BindView(R.id.registerMessage)
    TextView registerMessage;
    @BindView(R.id.username)
    TextInputLayout username;
    @BindView(R.id.password)
    TextInputLayout password;
    @BindView(R.id.repeatPassword)
    TextInputLayout repeatPassword;
    @BindView(R.id.email)
    TextInputLayout email;
    @BindView(R.id.name)
    TextInputLayout name;
    @BindView(R.id.surnames)
    TextInputLayout surnames;
    @BindView(R.id.birthDate)
    EditText birthDate;
    @BindView(R.id.genderPronoun)
    EditText genderPronoun;
    @BindView(R.id.register)
    MaterialButton register;
    @BindView(R.id.login)
    MaterialButton login;
    private RegisterViewModel mViewModel;


    private String[] optionsArray = {"Hombre","Mujer"};


    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(RegisterViewModel.class);






        /*
        Drawable iconaAlerta = Drawable.createFromPath("drawable/error_alert.xml");
        username.setError("Required Field");

        password.setError("Required Field");

        surnames.setError(getString(R.string.error_message));

        email.setError("Required Field");

         */


        //username.setEndIconDrawable(iconaAlerta);
    }

    @OnClick({R.id.register, R.id.login, R.id.birthDate, R.id.genderPronoun})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.register:
                goToLogged();
                break;
            case R.id.login:
                goBackToLogin();
                break;
            case R.id.birthDate:
                displayCalendar();
                break;
            case R.id.genderPronoun:
                chooseAnOption();
                break;
                
        }
    }

    private void chooseAnOption() {
        new MaterialAlertDialogBuilder(getContext())
                .setTitle(R.string.gender_pronoun)
                .setItems(optionsArray, this::onOptionClicked)
                .show();
    }

    private void onOptionClicked(DialogInterface dialogInterface, int i) {
        genderPronoun.setText(optionsArray[i]);
    }

    private void displayCalendar() {
        MaterialDatePicker.Builder<Long> builder = MaterialDatePicker.Builder.datePicker();
        builder.setTitleText(R.string.birth_date);
        MaterialDatePicker<Long> picker = builder.build();
        picker.addOnPositiveButtonClickListener(this::doOnDateSelected);
        picker.show(getFragmentManager(), picker.toString());
    }

    private void doOnDateSelected(Long aLong) { //convertir long a data

        String pattern = "dd-MM-yyyy";
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        String date = simpleDateFormat.format(new Date(aLong));
        birthDate.setText(date);

    }

    private void goToLogged() {

        validation();

    }

    private void validation() {

        boolean validate = true;

        refreshErrors();
        validate = validateIfEmpty(username, "El camp username està buit", validate);
        validate = validateIfEmpty(password, "El camp contrasenya està buit", validate);
        validate = validateIfEmpty(repeatPassword, "El camp repetir contrasenya està buit",validate);
        validate = validateIfEmpty(email, "El camp email està buit",validate);
        validate = validateEmailFormat(email, "Format incorrecte, no conté @", validate);
        validate = validateIfPasswordIsOk(repeatPassword, password, "Repeteix bé la contrasenya", validate);


        if (validate) {
            proceedToUserLogged();
        }

    }

    private boolean validateIfPasswordIsOk(TextInputLayout repeatPassword, TextInputLayout password, String solution, boolean validate) {
        if (!repeatPassword.getEditText().getText().toString().equals(password.getEditText().getText().toString())) {
            validate = false;
            repeatPassword.setError(solution);
            scrollTo(repeatPassword);

        }
        return validate;
    }

    private boolean validateEmailFormat(TextInputLayout email, String error, boolean validate) {
        if(!email.getEditText().getText().toString().contains("@")) {
            validate = false;
            email.setError(error);
            scrollTo(email);
        }
        return validate;
    }

    private boolean validateIfEmpty(TextInputLayout input, String error, boolean validate) {
        if(input.getEditText().getText().toString().isEmpty()) {
            validate = false;
            input.setError(error);
            scrollTo(input);

        }
        return validate;
    }

    private void refreshErrors() {
        username.setError("");
        password.setError("");
        repeatPassword.setError("");
        email.setError("");

    }

    private void proceedToUserLogged() {
        NavDirections action = RegisterFragmentDirections.toLoggedFromRegistered();
        Navigation.findNavController(getView()).navigate(action);
    }

    private void goBackToLogin() {
        NavDirections action = RegisterFragmentDirections.navigateToLogin();
        Navigation.findNavController(getView()).navigate(action);
    }

    private void scrollTo(TextInputLayout targetView) {
        targetView.getParent().requestChildFocus(targetView, targetView);
    }


}


