package cat.itb.userform.screens.welcome;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;

import com.google.android.material.button.MaterialButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cat.itb.userform.R;

public class WelcomeFragment extends Fragment {

    @BindView(R.id.backgroundImage)
    ImageView backgroundImage;
    @BindView(R.id.niceUserForm)
    TextView niceUserForm;
    @BindView(R.id.login)
    MaterialButton login;
    @BindView(R.id.register)
    MaterialButton register;
    private WelcomeViewModel mViewModel;


    public static WelcomeFragment newInstance() {
        return new WelcomeFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.welcome_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(WelcomeViewModel.class);
        // TODO: Use the ViewModel
    }

    @OnClick({R.id.login, R.id.register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login:
                navigateToLogin();
                break;
            case R.id.register:
                navigateToRegister();
                break;
        }
    }

    private void navigateToRegister() {
        NavDirections action = WelcomeFragmentDirections.navigateToRegister();
        Navigation.findNavController(getView()).navigate(action); //per no fer variables globals
    }

    private void navigateToLogin() {
        NavDirections action = WelcomeFragmentDirections.navigateToLogin();
        Navigation.findNavController(getView()).navigate(action);
    }
}
