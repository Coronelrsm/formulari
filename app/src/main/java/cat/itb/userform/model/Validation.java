package cat.itb.userform.model;

public class Validation {

    private String authToken;
    private String error;

    public String getAuthToken() {
        return authToken;
    }

    public String getError() {
        return error;
    }

    public boolean isLogged(){
        return authToken!=null;
    }
}
